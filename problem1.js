let problem1 = function(inventory, car_id){

    if(inventory == undefined || inventory.length == 0 || car_id == undefined || car_id.length == 0){
        return [];
    }

    let car = [];
    for (let i = 0, ii = inventory.length; i < ii; i++) {

        if(inventory[i].id && car_id.includes(inventory[i].id)){
            car.push(inventory[i]);
        }
    }

    return car;
}

module.exports = problem1;