let problem3 = function(inventory){

    if(inventory == undefined || inventory.length == 0){
        return [];
    }
    
    let car_model = [];

    for (let i = 0, ii = inventory.length; i < ii; i++) {

        if(inventory[i].car_model != undefined){
            car_model.push(inventory[i].car_model);
        }
        
    }
    
    car_model.sort(function(a,b){
        return a.localeCompare(b);
    });
    
    return car_model;
}

module.exports = problem3;