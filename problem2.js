let problem2 = function(inventory){
    
    if(inventory == undefined || inventory.length == 0){
        return [];
    } 
    
    let last_car = inventory.length-1;
    return ["Last car is a " + inventory[last_car].car_make + " " + inventory[last_car].car_model];
}

module.exports = problem2;