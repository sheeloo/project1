const inventory = require("./inventory.js");

const problem1 = require("./problem1.js");

let car_id = [33];

let car = problem1(inventory, car_id);

let result = [];

if(car.length > 0){
    for (let i = 0, ii = car.length; i < ii; i++) {

        result.push("Car " + car[i].id + " is a " + car[i].car_year + " " + car[i].car_make + " " + car[i].car_model);
    }
}

console.log(result);
