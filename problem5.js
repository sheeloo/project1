let problem5 = function(inventory, year){
    
    if(inventory == undefined || year == undefined || inventory.length == 0){
        return [];
    }

    let cars_older_than_the_year_2000 = [];

    for (let i = 0, ii = inventory.length; i < ii; i++) {

        if(inventory[i].car_year < year){
            cars_older_than_the_year_2000.push(inventory[i]);
        }
    }

    return cars_older_than_the_year_2000;

}

module.exports = problem5;