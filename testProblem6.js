const inventory = require("./inventory.js");

const problem6 = require("./problem6.js");

let car_make = ["BMW", "Audi"];
let BMW_and_Audi_cars = problem6(inventory, car_make);

let result = JSON.stringify(BMW_and_Audi_cars)

console.log(result);