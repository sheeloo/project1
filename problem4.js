let problem4 = function(inventory){
    
    if(inventory == undefined || inventory.length == 0){
        return [];
    }

    let car_years = [];
    for (let i = 0, ii = inventory.length; i < ii; i++) {
        if(inventory[i].car_year != undefined){
            car_years.push(inventory[i].car_year);
        }
    }
    return car_years;
}

module.exports = problem4;