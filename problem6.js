let problem6 = function(inventory, car_make){
    
    if(inventory == undefined || inventory.length == 0 || car_make == undefined || car_make.length == 0){
        return [];
    }

    let BMW_and_Audi_cars = [];
    
    for (let i = 0, ii = inventory.length; i < ii; i++) {

        if(inventory[i].car_make != undefined && car_make.includes(inventory[i].car_make)){
            BMW_and_Audi_cars.push(inventory[i].car_make);
        }
    }

    return BMW_and_Audi_cars;
}

module.exports = problem6;